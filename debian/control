Source: canu
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               architecture-is-64-bit [!x32],
               libboost-dev,
               libmeryl-dev,
               libsnappy-dev,
# For File::Path
               libfilesys-df-perl,
               mhap,
               debhelper
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/canu
Vcs-Git: https://salsa.debian.org/med-team/canu.git
Homepage: https://canu.readthedocs.org/en/latest/
Rules-Requires-Root: no

Package: canu
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${perl:Depends},
         libfilesys-df-perl,
         mhap,
         gnuplot-nox | gnuplot
Suggests: nanopolish
Description: single molecule sequence assembler for genomes
 Canu is a fork of the Celera Assembler, designed for high-noise
 single-molecule sequencing (such as the PacBio RS II or Oxford
 Nanopore MinION).
 .
 Canu is a hierarchical assembly pipeline which runs in four steps:
 .
  * Detect overlaps in high-noise sequences using MHAP
  * Generate corrected sequence consensus
  * Trim corrected sequences
  * Assemble trimmed corrected sequences
